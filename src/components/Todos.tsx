import React, { useState } from "react";

/**
 * JavaでいうInterfaceです。
 * ちなみにTypeScriptにもInterfaceは存在し、ほぼTypeと同じです。
 * 何が違うのかは`typescript interface type`とかで検索してみてください。
 */
type Todo = {
  sequenceId: number;
  content: string;
  isCompleted: boolean;
};

const initialState: Array<Todo> = [
  {
    sequenceId: 0,
    content: "Reactを学ぶ",
    isCompleted: false,
  },
  {
    sequenceId: 1,
    content: "TypeScriptを学ぶ",
    isCompleted: false,
  },
  {
    sequenceId: 2,
    content: "戦う",
    isCompleted: true,
  },
];

/**
 * 新たにsequenceIdを設定するときにこの関数を呼び出してください。
 * @param todos
 */
const getNewSequenceId = (todos: Array<Todo>) =>
  Math.max(...todos.map((todo) => todo.sequenceId)) + 1;

export default () => {
  const [todos, setTodos] = useState(initialState);

  return <></>;
};
