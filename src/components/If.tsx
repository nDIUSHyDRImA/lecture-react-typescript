import React from "react";

export default () => {
  const isDisplayed = true;

  return <>{isDisplayed && <p>Now you see me</p>}</>;
};
