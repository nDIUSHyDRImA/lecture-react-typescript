import React from "react";

type PropTypes = {
  parentMessage: string;
};

export default (props: PropTypes) => {
  const childMessage = "子のメッセージ";

  return (
    <>
      <p>{props.parentMessage}</p>
      <p>{childMessage}</p>
    </>
  );
};
