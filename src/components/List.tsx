import React from "react";

export default () => {
  const todos = ["Reactを教える", "TypeScriptを教える", "戦う"];

  return (
    <ul>
      {todos.map((todo) => (
        <li key={todo}>{todo}</li>
      ))}
    </ul>
  );
};
