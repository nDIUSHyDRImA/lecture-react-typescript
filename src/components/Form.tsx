import React, { useState } from "react";

export default () => {
  const [message, setMessage] = useState("なんらかのメッセージ");

  return (
    <>
      <p>{message}</p>
      <input
        type="text"
        value={message}
        onChange={(e) => setMessage(e.target.value)}
      />
    </>
  );
};
