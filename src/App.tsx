import React from "react";
import logo from "./logo.svg";
import "./App.css";
import HelloWorld from "./components/HelloWorld";
import Component from "./components/Component";
import If from "./components/If";
import List from "./components/List";
import Form from "./components/Form";
import Todos from "./components/Todos";

type Components = "HelloWorld" | "Component" | "If" | "List" | "Form" | "Todos";

/**
 * この定数を変更し、コンポーネントを切り替えてください
 */
const TARGET: Components = "HelloWorld";

function App() {
  const CurrentComponent = (() => {
    switch (TARGET) {
      case "HelloWorld":
        return <HelloWorld />;
      case "Component":
        return <Component parentMessage="親のメッセージ" />;
      case "If":
        return <If />;
      case "List":
        return <List />;
      case "Form":
        return <Form />;
      case "Todos":
        return <Todos />;
      default:
        throw new Error("どうやって到達したの？");
    }
  })();

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        {CurrentComponent}
      </header>
    </div>
  );
}

export default App;
